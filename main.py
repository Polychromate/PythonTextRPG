# 86 Inspired Python RPG v0.1

# Imports:

import os
from random import randint
from random import choice
from statistics import mean
import sys as sus
import time
import json
import pickle as pk 

def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)

# Variables

# Preliminary Classes:

    # 'Templates' for accounts
    
class account:
    
    def __init__(self, username, password, money):
        
        self.username = username
        self.password = password
        self.money = money

class unit:
    
    def __init__(self, maxHealth, health, skill, dmg):
        
        self.maxHealth = maxHealth
        self.health = health
        self.skill = skill
        self.dmg = dmg 
        
class legion(unit):
    pass
class friendly(unit):
    
    def __init__(self, maxHealth, health, skill, dmg, callsign):
        
        super().__init__(maxHealth, health, skill, dmg)
        
        self.callsign = callsign
        
class items:
    
    def __init__(self, itemid, name, value, marketable, description):
        
        self.itemid = itemid
        self.name = name
        self.value = value
        self.marketable = marketable
        self.description = description
        
    def sellItem(self, modifier):
        if self.marketable == True:
            return self.value * modifier

# Calculations for given units

def calculateBattle(friendlyUnits, legionUnits):
    
    # Gives a completely random number to decide who has the terrain advantage.
    
    terrainAdvantage = randint(0, 100)
    
    print(terrainAdvantage)
    
    friendlySkill = []
    legionSkill = []
    
    for each in friendlyUnits:
        friendlySkill.append(each.skill)
        print(friendlySkill)
        
    for each in legionUnits:
        legionSkill.append(each.skill)
        print(legionSkill)
        
    def attack(stkmodifier, atkmodifier, attackers):
        
        if attackers == "Self" or attackers == "Both":
            
            attackingFriendlyUnits = []
            
            for each in friendlyUnits:
                
                if (randint(int(each.skill), 100) * stkmodifier) <= 10:
                    
                    attackingFriendlyUnits.append(each)
                
            for each in attackingFriendlyUnits:
            
                dmg = atkmodifier * each.skill
                dmg = clamp(dmg, 0, 100)
                
                choice(legionUnits).health -= dmg
            
        if attackers == "Legion" or attackers == "Both":
            
            attackingLegionUnits = []
            
            for each in legionUnits:
                
                if (randint(int(each.skill), 100) * stkmodifier) <= 10:
                    
                    attackingLegionUnits.append(each)

            for each in attackingLegionUnits:
            
                dmg = each.dmg(atkmodifier * each.skill * 0.1)
                dmg = clamp(dmg, 0, each.maxHealth)
                
                choice(friendlyUnits).health -= dmg
            
    attack(1, 1, "Both")
     
    if mean(friendlySkill) > mean(legionSkill) and randint(1, 100) >= (terrainAdvantage/2):
        print("Using the terrain advantage as cover, your units confirmed \nvisual contact of the enemy units without being spotted")
    elif randint(1, 100) >= 90:
        print("Despite the terrain disadvantage, your units miraculously confirmed \nvisual contact of the enemy units without being spotted.")
        pass
    else: 
        print("Unfortunately, your units were spotted by the enemy swiftly")
    pass

# Prints the main menu for the first time

jim4 = friendly(health=100,maxHealth=100,skill=74,dmg=5,callsign="Jim Alpha")
jim3 = friendly(health=100,maxHealth=100,skill=92,dmg=5,callsign="Jim Beta")
jim1 = friendly(health=100,maxHealth=100,skill=44,dmg=5,callsign="Jim Omega")
jim2 = friendly(health=100,maxHealth=100,skill=63,dmg=5,callsign="Jim Pi")

calculateBattle([jim1, jim2], [jim3, jim4])

sus.exit()

print("\n86 Python RPG v0.1 \n")
print("1) Login\n")
print("2) Create Account\n")
print("3) Exit\n")
print("4) Play w/o account\n")

while True:

    os.system("CLS")

    print("\nGameplay\n")
    print("1) Intel\n")
    print("2) Military Actions\n")
    print("3) Units\n")
    print("4) Homebase\n")
    
    answer = input("Action #: ")
    
    while True:
        if answer == 1:

            while True:
                os.system("CLS")

                print("\nIntel Tab\n")
                print("1) Recon\n")
                print("2) Legion\n")
                print("3) Allies\n")
                
                answer = input("Action #: ")
                
                if answer == 1:

                    print("\nRecon Tab\n")
                    print("1) Captured Areas\n")
                    print("2) Explore Areas\n")
                    
                elif answer == 2:
                    
                    print("\nLegion Tab\n")
                    print("1) Legion Units\n")
                    print("2) Legion Bases\n")

                elif answer == 3:
                    
                    print("\nAllies Tab\n")
                    print("1) Allies Units\n")
                    print("2) Allies Bases\n")
                    print("3) Allies Relationns\n")

                else:

                    print("\nIvalid Response, please enter a number\n")

        elif answer == 2:
            while True:
                os.system("CLS")

                print("\nMilitary Actions Tab\n")
                print("1) Attack\n")
                print("2) Defense\n")

                answer = int(input("Input: "))

                if answer == 1:
                    os.system("CLS")

                    print("\nAttack Tab\n")
                    print("1) Set Attack\n")
                    print("2) Launch Attack\n")

                elif answer == 2:
                    os.system("CLS")

                    print("\nDefense Tab\n")
                    print("1) Set Defenses\n")
                    print("2) Deploy Defenses\n")
                else:
                    print("Invalid Input, please insert a number")
        elif answer == 3:
            while True:
                os.system("CLS")
                
                print("\nUnits Tab\n")
                print("1) View units\n")
                print("2) Recruit units\n")
                print("3) Retire units\n")
                print("4) Repair units\n")
        elif answer == 4:
            while True:
                os.system("CLS")

                print("\nHomebase tab\n")
                print("1) View Homebase Stats\n")
                print("2) View District Relations\n")
                print("3) Create Sub Base in District 86\n")
        else:
            pass